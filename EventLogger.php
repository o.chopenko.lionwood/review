<?php

namespace common\components;

use yii\base\Component;


class EventLogger extends Component{

	/**
	 * @var event logging service base url
	 */
	private $domain = 'refactored-domain';

	public function __construct()	{
	}

	/**
	 * @param $uri
	 * @param array $data
	 */
	private function sendRequest($uri, $data = [])	{
		$curl = curl_init();

		curl_setopt_array($curl, array(
			CURLOPT_URL => $this->domain . $uri,
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 30,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "POST",
			CURLOPT_POSTFIELDS => json_encode($data),
			CURLOPT_HTTPHEADER => array(),
		));

		$response = curl_exec($curl);
		curl_close($curl);
	}

	/**
	 * @param $user_id
	 * @param $auth_user_id
	 */
	public function logBAEnableUserStatus($user_id, $auth_user_id){
		$data = [
			'user_id' => (int)$user_id,
			'auth_user_id' => (int)$auth_user_id,
		];
		$this->sendRequest('user/status_enable', $data);
	}

	/**
	 * @param $user_id
	 * @param $auth_user_id
	 */
	public function logBADisableUserStatus($user_id, $auth_user_id){
		$data = [
			'user_id' => (int)$user_id,
			'auth_user_id' => (int)$auth_user_id,
		];
		$this->sendRequest('user/status_disable', $data);
	}

	/**
	 * @param $user_id
	 */
	public function logAdvisorStartSessionStatus($user_id){
		$data = [
			'user_id' => $user_id,
			'session_status' => 1,
		];
		$this->sendRequest('user/changed_session_status', $data);
	}

	/**
	 * @param $user_id
	 */
	public function logAdvisorEndSessionStatus($user_id){
		$data = [
			'user_id' => $user_id,
			'session_status' => 0,
		];
		$this->sendRequest('user/changed_session_status', $data);
	}

	/**
	 * @param $user_id
	 */
	public function logSystemEndAdvisorSession($user_id){
		$data = [
			'user_id' => $user_id
		];
		$this->sendRequest('user/end_session_status_by_system', $data);
	}

	/**
	 * @param $user_id
	 * @param $ip
	 * @param $url
	 */
	public function logNonCustomerLogIn($user_id, $ip, $url){
		$data = [
			'user_id' => $user_id,
			'ip' => $ip,
			'url' => $url
		];
		$this->sendRequest('user/login_success', $data);
	}

	/**
	 * @param $user_id
	 * @param $auth_user_id
	 * @param $rate
	 */
	public function logAPPMChanges($user_id, $auth_user_id, $rate){

		$data = [
			'user_id' => (int)$user_id,
			'auth_user_id' => (int)$auth_user_id,
			'rate' => $rate
		];
		$this->sendRequest('user/change_appm_rate', $data);
	}


	/**
	 * @param $user_id
	 * @param $auth_user_id
	 * @param $rate
	 */
	public function logGPPMChanges($user_id, $auth_user_id, $rate){

		$data = [
			'user_id' => (int)$user_id,
			'auth_user_id' => (int)$auth_user_id,
			'rate' => $rate
		];
		$this->sendRequest('user/change_gppm_rate', $data);
	}



	/**
	 * @param $user_id
	 * @param $ip
	 */
	public function logCustomerLogIn($user_id, $ip){
		$data = [
			'user_id' => $user_id,
			'ip' => $ip
		];
		$this->sendRequest('customer/login_success', $data);
	}

	/**
	 * @param $user_id
	 */
	public function logCustomerVerifiedCallbackNumber($user_id){
		$data = [
			'user_id' => $user_id
		];
		$this->sendRequest('customer/callback_number', $data);
	}

	/**
	 * @param $user_id
	 * @param $phone - bool
	 * @param $country_code - bool
	 * @param $phone_type - bool
	 */
	public function logCustomerVerifiedUpdateCallbackNumber($user_id, $phone, $country_code, $phone_type){
		$items = [];
		if ($phone)
			$items[] = 'callback_number';

		if ($country_code)
			$items[] = 'callback_country';

		if ($phone_type)
			$items[] = 'number_type';

		$data = [
			'user_id' => $user_id,
			'items' => $items
		];

		$this->sendRequest('customer/callback_number_update', $data);
	}

	/**
	 * @param $user_id
	 * @param $auth_user_id
	 */
	public function logCSCustomerStatusEnabled($user_id, $auth_user_id){
		$data = [
			'user_id' => (int)$user_id,
			'auth_user_id' => (int)$auth_user_id
		];
		$this->sendRequest('customer/status_enable', $data);
	}

	/**
	 * @param $user_id
	 * @param $auth_user_id
	 * @param $reason
	 */
	public function logCSCustomerStatusDisabled($user_id, $auth_user_id, $reason){
		$data = [
			'user_id' => (int)$user_id,
			'auth_user_id' => (int)$auth_user_id,
			'reason' => $reason
		];
		$this->sendRequest('customer/status_disable', $data);
	}

	/**
	 * @param $user_id
	 * @param $auth_user_id
	 */
	public function logFCustomerStatusEnabled($user_id, $auth_user_id){
		$data = [
			'user_id' => (int)$user_id,
			'auth_user_id' => (int)$auth_user_id
		];
		$this->sendRequest('customer/status_enable', $data);
	}

	/**
	 * @param $user_id
	 * @param $auth_user_id
	 * @param $reason
	 */
	public function logFCustomerStatusDisabled($user_id, $auth_user_id, $reason){
		$data = [
			'user_id' => (int)$user_id,
			'auth_user_id' => (int)$auth_user_id,
			'reason' => $reason
		];
		$this->sendRequest('customer/status_disable', $data);
	}

	/**
	 * @param $user_id
	 * @param $ip
	 */
	public function logCustomerCreateAccount($user_id, $ip){
		$data = [
			'user_id' => $user_id,
			'ip' => $ip
		];
		$this->sendRequest('customer/create_account', $data);
	}

	/**
	 * @param $user_id
	 * @param $first_name -bool
	 * @param $last_name - bool
	 * @param $email - bool
	 * @param $newsletter - bool
	 */
	public function logCustomerUpdateAccount($user_id, $first_name, $last_name, $email, $newsletter){
		$items = [];
		if ($first_name)
			$items[] = 'first_name';

		if ($last_name)
			$items[] = 'last_name';

		if ($email)
			$items[] = 'email';

		if ($newsletter)
			$items[] = 'newsletter';

		$data = [
			'user_id' => $user_id,
			'items' => $items
		];
		$this->sendRequest('customer/update_account', $data);
	}

	/**
	 * @param $user_id
	 * @param $auth_user_id
	 * @param $memo
	 */
	public function logCSUpdateCustomerMemo($user_id, $auth_user_id, $memo){
		$data = [
			'user_id' => (int)$user_id,
			'auth_user_id' => (int)$auth_user_id,
			'memo' => $memo,
		];
		$this->sendRequest('customer/update_memo', $data);
	}

	/**
	 * @param $user_id
	 * @param $auth_user_id
	 * @param $memo
	 */
	public function logFUpdateCustomerMemo($user_id, $auth_user_id, $memo){
		$data = [
			'user_id' => (int)$user_id,
			'auth_user_id' => (int)$auth_user_id,
			'memo' => $memo,
		];
		$this->sendRequest('customer/update_memo', $data);
	}

}