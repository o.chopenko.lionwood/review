<?php

namespace mentor\modules\v1\controllers;

use common\filters\auth\HttpBearerAuth;

use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\AccessControl;
use yii\filters\auth\CompositeAuth;
use yii\helpers\Url;
use yii\rest\ActiveController;
use yii\web\HttpException;
use yii\web\NotFoundHttpException;
use yii\web\ServerErrorHttpException;
use mentor\models\User;

use mentor\models\form\LoginForm;

use mentor\models\form\PhoneCheckForm;

use common\components\AccessRule;

class MentorController extends ActiveController
{
    public $modelClass = 'mentor\models\User';

    public function __construct($id, $module, $config = [])
    {
        parent::__construct($id, $module, $config);
    }

    public function actions()
    {
        return [
            'index' => [
                'class' => 'yii\rest\IndexAction',
                'modelClass' => $this->modelClass,
                'prepareDataProvider' => function () {
                    $searchModel = new \mentor\models\search\UserSearch();
                    return $searchModel->search(Yii::$app->request->queryParams);
                },
            ],
            'view' => [
                'class' => 'yii\rest\ViewAction',
                'modelClass' => $this->modelClass,
                'checkAccess' => [$this, 'checkAccess'],
            ],
        ];
    }

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => CompositeAuth::className(),
            'authMethods' => [
                HttpBearerAuth::className(),
            ],
        ];
        $behaviors['verbs'] = [
            'class' => \yii\filters\VerbFilter::className(),
            'actions' => [
                'index'=>['get'],
                'view'=>['get'],
                'login' => ['post'],
                'me' => ['get', 'post'],
                'check-phone-user'=>['post'],
            ],
        ];

        // remove authentication filter
        $auth = $behaviors['authenticator'];
        unset($behaviors['authenticator']);

        // add CORS filter
        $behaviors['corsFilter'] = [
            'class' => \yii\filters\Cors::className(),
            'cors' => [
                'Origin' => ['*'],
                'Access-Control-Request-Method' => ['GET', 'POST', 'PUT', 'DELETE', 'OPTIONS'],
                'Access-Control-Request-Headers' => ['*'],
            ],
        ];

        // re-add authentication filter
        $behaviors['authenticator']           = $auth;
        // avoid authentication on CORS-pre-flight requests (HTTP OPTIONS method)
        $behaviors['authenticator']['except'] = ['options', 'login',
            'check-phone','check-by-email','index','view'];

        // setup access
        $behaviors['access'] = [
            'class' => AccessControl::className(),
            'ruleConfig' => [
                    'class' => AccessRule::className(),
                ],
            'only' => ['me'], //only be applied to
            'rules' => [
                [
                    'allow' => true,
                    'actions' => ['me'],
                    'roles' => [User::ROLE_MENTOR]
                ]
            ],
        ];

        return $behaviors;
    }

    /**
     * Rest Description: Mentors list (active).
     */
    public function actionIndex()
    {
    }

    /**
     * Rest Description: Mentor view (active).
     */
    public function actionView()
    {

    }

    /**
     * Rest Description: Login action for mentor.
     * Rest Fields: ['LoginForm'=>['phone','confirmation_code']].
     */
    public function actionLogin()
    {
        $model        = new LoginForm();
        $model->roles = [
            User::ROLE_MENTOR
        ];
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            $user = $model->getUser();
            $user->generateAccessTokenAfterUpdatingClientInfo(true);

            $response = \Yii::$app->getResponse();
            $response->setStatusCode(200);
            $id       = implode(',', array_values($user->getPrimaryKey(true)));

            $responseData = [
                'id' => (int) $id,
                'access_token' => $user->access_token,
            ];

	    \Yii::$app->EventLogger->logNonCustomerLogIn($user->id, \Yii::$app->request->userIP, Yii::$app->request->getOrigin());

            return $responseData;
        } else {
            // Validation error
            throw new HttpException(422, json_encode($model->errors));
        }
    }

    /**
     * Rest Description: Check phone action for mentor for login (scenario=>'login').
     * Rest Fields: ['PhoneCheckForm'=>['phone', 'call_type','scenario']].
     */
    public function actionCheckPhone()
    {
        $model = new PhoneCheckForm();
        if (Yii::$app->request->post('PhoneCheckForm')['scenario'] === 'confirm') {
            $model->scenario = 'confirm';
        }
        if (Yii::$app->request->post('PhoneCheckForm')['scenario'] === 'login') {
            $model->scenario = 'login';
        }
        if (Yii::$app->request->post('PhoneCheckForm')['scenario'] === 'update') {
            $model->scenario = 'update';
        }
        if ($model->load(Yii::$app->request->post()) && $model->confirm()) {
            $response = \Yii::$app->getResponse();
            $response->setStatusCode(200);

            $responseData = true;

            return $responseData;
        } else {
            // Validation error
            throw new HttpException(422, json_encode($model->errors));
        }
    }


    /**
     * Rest Description: Check action for mentor by email.
     * Rest Fields: ['CheckByEmail'=>['phone']].
     */
    public function actionCheckByEmail()
    {
        $model = new \mentor\models\form\CheckByEmail();

        if ($model->load(Yii::$app->request->post()) && $model->sendEmail()) {

            $response = \Yii::$app->getResponse();
            $response->setStatusCode(200);
            $responseData = true;

            return $responseData;
        } else {
            // Validation error
            throw new HttpException(422, json_encode($model->errors));
        }
    }


    /**
     * Rest Description: Get information about current logged mentor. Required authorization token.
     * Rest Expand: ['info', 'rate','mentorStatus'].
     */
    public function actionMe()
    {
        if ($user = User::findIdentity(\Yii::$app->user->getId())) {
            $response = \Yii::$app->getResponse();
            $response->setStatusCode(200);

            return $user;
        } else {
            // Validation error
            throw new NotFoundHttpException("Object not found");
        }
    }

    public function actionOptions($id = null)
    {
        return "ok";
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        // check if the user can access $action and $model
        // throw ForbiddenHttpException if access should be denied
        if ($action === 'view') {
            if ($model->role !== User::ROLE_MENTOR)
                    throw new \yii\web\ForbiddenHttpException(sprintf('You can only %s mentors.',
                    $action));
        }
    }
}