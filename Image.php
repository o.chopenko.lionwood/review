<?php

namespace common\models;

use Yii;
use yii\behaviors\TimestampBehavior;
use common\behaviors\UploadBase64;

/**
 * This is the model class for table "image".
 *
 * @property int $id
 * @property int $status
 * @property string $title
 * @property string $image
 * @property int $public
 * @property int $created_at
 * @property int $updated_at
 */
class Image extends \yii\db\ActiveRecord
{
    const IMAGE_PUBLIC  = 1;
    const IMAGE_PRIVATE = 0;

    const STATUS_ACTIVE = 1;
    const STATUS_DISABLED = 0;

    const SCENARIO_UPDATE = 'update';
    const SCENARIO_CREATE = 'create';

    public $upload;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'image';
    }

    public function fields()
    {
        $fields = parent::fields();
        unset($fields['created_at']);
        unset($fields['updated_at']);
        return $fields;
    }

    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios[self::SCENARIO_CREATE] = ['public','status','title','image','upload'];
        $scenarios[self::SCENARIO_UPDATE] = ['public','status','title','image','upload'];
        return $scenarios;
    }


    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['public','status','title'],'required'],
            [['public'],'in','range'=>[self::IMAGE_PRIVATE,self::IMAGE_PUBLIC],'strict'=>true],
            [['status'],'in','range'=>[self::STATUS_ACTIVE,self::STATUS_DISABLED],'strict'=>true],
            [['status', 'public'], 'integer'],
            [['title', 'image'], 'string', 'max' => 255],
            [['upload'],'required','on'=>self::SCENARIO_CREATE],
            [['upload'],'safe','on'=>self::SCENARIO_UPDATE],
        ];
    }

     /** @inheritdoc */
    public function behaviors()
    {
        return [
            [
                'class' =>  TimestampBehavior::className(),
                'createdAtAttribute' => 'created_at',
                'updatedAtAttribute' => 'updated_at',
            ],
            [
                'class' => UploadBase64::class,
            ],
        ];
    }

    /**
     * {@inheritdoc}
     * @return \common\models\query\ImageQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new \common\models\query\ImageQuery(get_called_class());
    }
}